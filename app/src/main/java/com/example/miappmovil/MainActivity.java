package com.example.miappmovil;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.miappmovil.Model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    private EditText txtcorrreo, txtcontraseña;
    private Button btniniciarsecion, btnregistraruser, admin;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        txtcorrreo = (EditText) findViewById(R.id.txtcoorreo);
        txtcontraseña = (EditText) findViewById(R.id.txtcontraseñaa);
        btniniciarsecion = (Button) findViewById(R.id.btniniciarsecion);
        btnregistraruser = (Button) findViewById(R.id.btncrearcuenta);
        admin = findViewById(R.id.admin);
        auth = FirebaseAuth.getInstance();
        //INIT FIREBASE

        admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Homme.class));
            }
        });
        btnregistraruser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registro = new Intent(MainActivity.this, Registro_Activity.class);
                startActivity(registro);

            }
        });
        btniniciarsecion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ProgressDialog mDialod = new ProgressDialog(MainActivity.this);
                mDialod.setMessage("Espere");
                mDialod.show();//pogres
                String txt_correo = txtcorrreo.getText().toString();
                if (isValidEmail(txt_correo) && validarContraseña()) {
                    String txt_contrasena = txtcontraseña.getText().toString();
                    auth.signInWithEmailAndPassword(txt_correo, txt_contrasena).
                            addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {

                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        Toast.makeText(MainActivity.this, "se ingreso correctamente", Toast.LENGTH_SHORT).show();
                                        nextActivity();


                                    } else {
                                        // If sign in fails, display a message to the user.
                                        Toast.makeText(MainActivity.this, "ocurrio un error al inicio de sesion", Toast.LENGTH_SHORT).show();

                                    }
                                }
                            });

                } else {
                    Toast.makeText(MainActivity.this, "no se puede iniciar sesion", Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    public void onDataChange(DataSnapshot dataSnapshot) {//
    User user = dataSnapshot.child(txtcorrreo.getText().toString()).getValue(User.class);}//

    private boolean isValidEmail (CharSequence target){
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
    public boolean validarContraseña (){
        String contraseña;
        contraseña = txtcontraseña.getText().toString();
        if (contraseña.length() >=6){
            return true;
        }else return false;
    }
    @Override
    protected void onResume() {
        super.onResume();
        FirebaseUser currentUser = auth.getCurrentUser();
        if (currentUser != null){
            Toast.makeText(this,"Usuario logeado",Toast.LENGTH_SHORT).show();
            nextActivity();
        }else { }

    }
    private void nextActivity(){
        startActivity(new Intent(MainActivity.this, Navegacion_Activity.class));
        finish();

    }


}



