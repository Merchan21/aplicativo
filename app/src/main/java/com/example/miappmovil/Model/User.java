package com.example.miappmovil.Model;

public class User {
    private String IsStaff;
    private String nombre;
    private String apellidos;
    private String direccion;
    private String idUsuario;
    private String telefono;
    private String correo;

    public User() {
    }

    public User(String isStaff, String nombre, String apellidos, String direccion, String idUsuario, String telefono, String correo) {
        IsStaff = isStaff;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.idUsuario = idUsuario;
        this.telefono = telefono;
        this.correo = correo;

    }

    public String getIsStaff() {
        return IsStaff;
    }

    public void setIsStaff(String isStaff) {
        IsStaff = isStaff;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
}