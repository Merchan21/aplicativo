package com.example.miappmovil;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.miappmovil.Model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class Registro_Activity extends AppCompatActivity {

    private EditText txtnombre, txtapellido, txtdireccion, txttelefono, txtcorreo, txtcontraseña, contraseñaRepetida;
    private Button btnregistro;

    private FirebaseAuth mAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private Uri uri;

    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_);

        progressDialog = new ProgressDialog(this);

        txtnombre = (EditText) findViewById(R.id.txtNombres);
        txtapellido = (EditText) findViewById(R.id.txtApellido);
        txtdireccion = (EditText) findViewById(R.id.txtDireccion);
        txttelefono = (EditText) findViewById(R.id.txttelefono);
        txtcorreo = (EditText) findViewById(R.id.txtCorreo);
        txtcontraseña = (EditText) findViewById(R.id.txtContraseña);
        contraseñaRepetida = (EditText) findViewById(R.id.txtRcontraseña);
        btnregistro = (Button) findViewById(R.id.btnRegistrarme);
        uri = null;


        //para ir atras
        setupActionBar();
        mAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("Usuario");

        btnregistro.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                progressDialog.setMessage("Realizando registro");
                progressDialog.show();
                if (txtnombre.getText().toString().isEmpty()) {
                    txtnombre.setError("INGRESE NOMBRE");
                    txtnombre.requestFocus();
                } else if (txtapellido.getText().toString().isEmpty()) {
                    txtapellido.setError("INGRESE APELLIDO");
                    txtapellido.requestFocus();
                } else if (txtdireccion.getText().toString().isEmpty()) {
                    txtdireccion.setError("INGRESE DIRECCION");
                    txtdireccion.requestFocus();
                } else if (txtcorreo.getText().toString().isEmpty()) {
                    txtcorreo.setError("INGRESE CORREO");
                    txtcorreo.requestFocus();
                } else if (txttelefono.getText().toString().isEmpty()) {
                    txttelefono.setError("INGRESE TELEFONO");
                    txttelefono.requestFocus();
                } else if (!validarContraseña()) {
                    txtcontraseña.setError("CONTRASEÑA NO COINCIDEN");
                    txtcontraseña.requestFocus();
                }
                mAuth.createUserWithEmailAndPassword(txtcorreo.getText().toString(), txtcontraseña.getText().toString())
                        .addOnCompleteListener(Registro_Activity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    User usuario = new User();
                                    usuario.setCorreo(txtcorreo.getText().toString());
                                    usuario.setNombre(txtnombre.getText().toString());
                                    usuario.setApellidos(txtapellido.getText().toString().trim());
                                    usuario.setDireccion(txtdireccion.getText().toString().trim());
                                    usuario.setTelefono(txttelefono.getText().toString());
                                    usuario.setIdUsuario(FirebaseAuth.getInstance().getCurrentUser().getUid());


                                    databaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(usuario)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    Toast.makeText(Registro_Activity.this, "se registro correctamente", Toast.LENGTH_SHORT).show();
                                                    finish();
                                                }
                                            }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(Registro_Activity.this, "Nose ha podido registrar el usuario", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                } else {
                                    Toast.makeText(Registro_Activity.this, "EL CORREO CON EL QUE SE INTENTA REGISTRAR YA EXISTE", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });


            }
        });
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Registro de usuario");
        }

    }

    private boolean validarContraseña() {
        String Contraseña,ContraseñaRepetida;
        Contraseña = txtcontraseña.getText().toString();
        ContraseñaRepetida = contraseñaRepetida.getText().toString();
        if (Contraseña.equals(ContraseñaRepetida)){
            if (Contraseña.length() >=6){
                return true;
            }else return false;
        }else return false;

    }
}
